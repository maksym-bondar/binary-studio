import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const { name } = fighter;

  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  const fighterImgElement = createFighterImage(fighter);

  bodyElement.append(fighterImgElement);
  showModal({
    title: `${name} WIN!`,
    bodyElement,
    onClose: () => location.reload(),
  });
}
