import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const initialFirstFighterHealth = firstFighter.health;
  const initialSecondFighterHealth = secondFighter.health;

  return new Promise((resolve) => {
    let winner;
    let keysPressed = {};
    let isOnePlayerComboTimerStart = false;
    let isTwoPlayerComboTimerStart = false;

    window.addEventListener('keydown', action);
    window.addEventListener('keyup', action);

    function action(e) {
      keysPressed[e.code] = e.type == 'keydown';

      const isOnePlayerComboAvailable =
        controls.PlayerOneCriticalHitCombination.filter((key) => keysPressed[key]).length === 3 &&
        !isOnePlayerComboTimerStart;
      const isTwoPlayerComboAvailable =
        controls.PlayerTwoCriticalHitCombination.filter((key) => keysPressed[key]).length === 3 &&
        !isTwoPlayerComboTimerStart;
      const isOnePlayerAttackAvailable =
        keysPressed[controls.PlayerOneAttack] &&
        !keysPressed[controls.PlayerOneBlock] &&
        !keysPressed[controls.PlayerTwoBlock] &&
        !isOnePlayerComboAvailable;
      const isTwoPlayerAttackAvailable =
        keysPressed[controls.PlayerTwoAttack] &&
        !keysPressed[controls.PlayerTwoBlock] &&
        !keysPressed[controls.PlayerOneBlock] &&
        !isTwoPlayerComboAvailable;

      if (firstFighter.health <= 0) {
        winner = secondFighter;
      }

      if (secondFighter.health <= 0) {
        winner = firstFighter;
      }

      if (winner) {
        return resolve(winner);
      }

      if (isOnePlayerAttackAvailable) {
        secondFighter.health -= getDamage(firstFighter, secondFighter);
        showHealthBar('right', initialSecondFighterHealth, secondFighter.health);
      }

      if (isTwoPlayerAttackAvailable) {
        firstFighter.health -= getDamage(secondFighter, firstFighter);
        showHealthBar('left', initialFirstFighterHealth, firstFighter.health);
      }

      if (isOnePlayerComboAvailable) {
        isOnePlayerComboTimerStart = true;
        comboAttack(firstFighter, secondFighter).then((res) => (isOnePlayerComboTimerStart = !!res));
        showHealthBar('right', initialSecondFighterHealth, secondFighter.health);
      }

      if (isTwoPlayerComboAvailable) {
        isTwoPlayerComboTimerStart = true;
        comboAttack(secondFighter, firstFighter).then((res) => (isOnePlayerComboTimerStart = !!res));
        showHealthBar('left', initialFirstFighterHealth, firstFighter.health);
      }
    }
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);

  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() * (2 - 1) + 1;
  const power = criticalHitChance * fighter.attack;

  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() * (2 - 1) + 1;
  const power = fighter.defense * dodgeChance;

  return power;
}

function showHealthBar(position, initial, current) {
  const barElement = document.getElementById(`${position}-fighter-indicator`);
  let barWidth = (current * 100) / initial;

  barElement.style.width = `${barWidth < 0 ? 0 : barWidth}%`;
}

function comboAttack(attacker, defender) {
  return new Promise((resolve) => {
    let timer = 0;

    let interval = setInterval(function () {
      timer++;

      if (timer == 10) {
        clearInterval(interval);
        timer = 0;
        resolve(timer);
      }
    }, 1000);

    defender.health -= 2 * attacker.attack;
  });
}
