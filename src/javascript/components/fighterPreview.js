import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const imgElement = createFighterImage(fighter);
    const detailsElement = createFighterDetails(fighter);

    fighterElement.append(imgElement);
    fighterElement.prepend(detailsElement);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

const createFighterDetails = (fighter) => {
  const { attack, defense, health, name } = fighter;

  const fighterDetails = {
    name,
    health,
    attack,
    defense,
  };

  const detailElement = createElement({ tagName: 'div', className: 'fighter-details' });

  const renderDetails = () =>
    Object.keys(fighterDetails).map(
      (detail) => `<p class='info-row'><strong>${detail}</strong>: ${fighter[detail]}</p>`
    );

  detailElement.innerHTML = renderDetails().join(' ');

  return detailElement;
};
